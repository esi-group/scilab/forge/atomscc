package AtomsCC::SysUtils;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#

use strict;
use Exporter;
use Cwd;
use File::Basename;
use File::Path;
use File::Copy;

use AtomsCC::Log;

our @ISA    = ('Exporter');
our @EXPORT = qw(&deleteDir &copyDir &timer &find_scilab);


# returns the path to the Scilab executable to use with "-nw" option;
# it may not work for other options.
sub find_scilab
{
    if (defined($::scilab)) {
        return $::scilab;
    }

    if ( !defined($::scilab_path) ){
        $::scilab_path = $::DEFAULT_SCILAB_PATH;
    }

    my $install;
    if($::WINDOWS) {
        $install = $::scilab_path . $::sep . 'scilab-' . $::scilab_version;
        $::scilab = $install.'\\bin\\';
        if($::scilab_version =~ /^5\./) {
            $::scilab .= 'Scilex.exe';
        }
        else {
            # in 6.0, '-nw' requires WScilex.exe
            $::scilab .= 'WScilex.exe';
        }
    }
    elsif($::LINUX )
    {
        $install = $::scilab_path . $::sep . 'scilab-' . $::scilab_version;
        $::scilab = $install.'/bin/scilab';
    }
    elsif( $::MACOSX ) {
        $install = $::scilab_path . $::sep . 'scilab-' . $::scilab_version.'.app';
        $::scilab = $install.'/Contents/MacOS/bin/scilab';
    }

    if (! -e $::scilab) {
        error("No Scilab found at $install. Please install Scilab ".$::scilab_version." at that location or specify a scilab installation path with --scilab-path (or -P) option.");
    }

    return $::scilab;
}

# ==================================================================================

## @fn void deleteDir($directory)
# @author Pierre MARECHAL
# @date 13 mars 2007
#
# @par Description:
#   Suppression du contenu du répertoire passé en paramètre

# Renvoie 0 si tout le contenu du répertoire a été supprimé,
# Sinon, renvoie le nom du fichier ou répertoire qui n'a pu être supprimé

# ==================================================================================

sub deleteDir
{
    my $deleteDirectory = $_[0];
    my $returnCode      = 1;
    my $delete_totally = 0;
    if($#_ == 1){
    	$delete_totally = 1;
    }
    # go to the directory to remove
    unless( chdir($deleteDirectory) )
    {
    	if(! $delete_totally){
            mkdir($deleteDirectory,0777);
        }
        return 1;
    }

    my @entries = <*>;

    foreach my $entry (@entries)
    {
        if( -f $entry ){ unlink($entry); }
        if( -d $entry ){ rmtree($entry); }
        if( -e $entry ){ return 0;       }
    }
    if($delete_totally){
		my $dir = basename($deleteDirectory);
		my $parent = dirname($deleteDirectory);
		chdir($parent);
		rmdir($dir);
    }

    return 1;
}



# ==============================================================================
# Author : Pierre MARECHAL
# Date   : 19 oct 2004
#
# Fonction : Copie l'intégralité du repertoire passé en paramètre
# ==============================================================================

sub copyDir
{
    my $from_dir     = $_[0];
    my $to_dir       = $_[1];
    my $only_subdir = 0;

    # Only copy subdir files/dirs?
    if($#_ >= 2){
    	$only_subdir = 1;
    }

    my $from_subdir  = basename($from_dir);

    my $DIR_TO_SAVE  = getcwd();

    unless( -e $to_dir )
    {
        # Le répertoire $to_subdir n'existe pas
        # =====================================
        mkdir($to_dir,0777) || return 0;
    }
    elsif( ! (-e $to_dir.$::sep.$from_subdir) && ! $only_subdir )
    {
        # Création du répertoire $from_subdir s'il n'existe pas
        # =====================================================
        mkdir($to_dir.$::sep.$from_subdir,0777) || return 0;
        $to_dir .= $::sep.$from_subdir;
    }
    chdir($from_dir) || return 0;

    my @entries = <*>;
    my $entry;

    foreach $entry ( @entries )
    {
        if(-d $entry)
        {
            unless( copyDir($from_dir.$::sep.$entry,
                            $to_dir  .$::sep.$entry) )
            {
                return 0;
            }
        }
        elsif(-f $entry)
        {
            unless( copy($from_dir.$::sep.$entry,
                         $to_dir.$::sep.$entry) )
            {
                return 0;
            }

            if( -X $from_dir.$::sep.$entry )
            {
                chmod 0755, $to_dir.$::sep.$entry;
            }
        }
    }

    chdir($DIR_TO_SAVE) || return 0;
    return 1;
}

sub timer
{
    my $reason            = $_[0];
    my $Dir               = $_[1];      # running directory
    my $commandToRun      = $_[2];      # command to start
    my $logfile           = $_[3];
    my $timeout           = $_[4];      # in minutes

    my $return_code       = 0;

    if ( !defined($timeout) ) {
        $timeout = 10;
    }
    if ( $::NOTIMEOUT ) {
        info("Running $reason without timeout (it may never finish). See progress in $logfile.");
    }
    else {
        info("Running $reason; it may take up to $timeout minutes. See progress in $logfile.");
    }
    verbose("Command is: ".$commandToRun);
    verbose("(in directory ".$Dir.")");
    verbose("If it is hung, you should be able to stop it by typing: <ctrl-C> <enter> abort <enter> exit <enter>");
    if ( ! $::NOTIMEOUT ) {
        $SIG{ALRM} = sub { die 'timeout' };
    }

    eval {
        if ( ! $::NOTIMEOUT ) {
            alarm($timeout * 60);
        }
        chdir($Dir);
        $return_code = system($commandToRun.'> '.$logfile.' 2>&1');

        if ( ! $::NOTIMEOUT ) {
            alarm(0);
        }
    };

    if($@)
    {
        if($@ =~ /timeout/)
        {
            error("Timeout while running '$commandToRun'. The process may still be running; you'll have to kill it by hand. Then check the output file, and possibly rerun with '--notimeout'.");

            # TODO: re-put a way to kill the process after a timeout.
            # Currently it'll continue running...
            # I've removed the code to kill processes,
            # because it would kill **all*** processes:
            # A little too brutal...
        }
        else
        {
            error("Error while running '$commandToRun': ".$@);
        }
    }

    # Shift by 8 to obtain the system return status code
    # and forget about the first 8 bits of signal id
    return ($return_code>>8);
}

1;
