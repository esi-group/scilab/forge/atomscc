package AtomsCC::Test;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#

use strict;
use Exporter;
use File::Path;
use File::Basename;
use File::Copy;

use AtomsCC::Log;
use AtomsCC::SysUtils;
use AtomsCC::Clean;

our @ISA = ('Exporter');
our @EXPORT = qw( &stage_Test );

sub stage_Test
{
    my @dependencies;
    foreach my $key (sort keys %::toolbox ) {
        if($key =~ m/depends_[0-9]+/ ) {
            # dependencies are in format "~ <toolbox name> any"
            # ("~" and "any" are unused, legacy stuff)
            my @tmp = split(" ",$::toolbox{$key});
            if ( ! (($tmp[0] eq '~') && ($tmp[2] eq 'any')) )
            {
                warning("Invalid dependency string '".$::toolbox{$key}."' found for toolbox ".$::toolbox{'name'}.", skipped");
                next;
            }
            push(@dependencies,$tmp[1]);
        }
    }

    # Create the script
    my $test_script_content = '';
    # TODO: in scilab 6, don't use %onprompt which is really complex; just adding "-quit" as option should be enough.
    $test_script_content .= 'function %onprompt()'."\n";
    $test_script_content .= '  exit(2)'."\n";
    $test_script_content .= 'endfunction'."\n";
    $test_script_content .= ''."\n";
    $test_script_content .= ''."\n";
    $test_script_content .= 'atomsSetConfig("offLine","False");'."\n";
    #$test_script_content .= 'atomsSystemUpdate();'."\n"; # TODO: this is not needed, 
    # because the 'atomsSetConfig("offLine","False")' already does an atomsSystemUpdate ! but 
    # that's a bug and should be fixed.
    foreach my $depend (@dependencies) {
        $test_script_content .= 'atomsInstall("'.$depend.'");'."\n";
        $test_script_content .= 'atomsLoad("'.$depend.'");'."\n";
    }
    
    $test_script_content .= 'atomsSetConfig("offLine","True");'."\n";
    $test_script_content .= 'atomsInstall("'.$::binary.'");'."\n";
    $test_script_content .= 'atomsLoad("'.$::toolbox_name.'");'."\n";
    $test_script_content .= 'disp("Toolbox '.$::toolbox_name.' loaded at " + atomsGetLoadedPath("'.$::toolbox_name.'"));'."\n";
    $test_script_content .= 'exit_code = 0;'."\n";
    $test_script_content .= 'if ~test_run("'.$::toolbox_name.'", [], "no_check_error_output") then exit_code = 1, end;'."\n";
    $test_script_content .= 'atomsRemove("'.$::toolbox_name.'");'."\n";
    $test_script_content .= 'exit(exit_code)';
    $::test_script = $::work.$::sep."scripts".$::sep.$::toolbox_name.".sce";
    unless (open (TEST_SCRIPT,"> ".$::test_script)) {
        error('Failed to create test script in '.$::test_script);
    }

    verbose('Creating test script in '.$::test_script);
    print TEST_SCRIPT $test_script_content;
    close TEST_SCRIPT;
    $test_script_content =~ s/^/  /gm;
    verbose('Test script contains:
'.$test_script_content);

    my $build_cmd = '"'.find_scilab().'" -nw -nb -f '.$::test_script;

    my $return_code = timer('test script', dirname($::test_script), $build_cmd, $::test_log);
    if ($return_code == 0) {
        info("Tests finished without errors; please also check the log file at $::test_log");
    }
    elsif ($return_code == 1) {
        warning("Test run failed; please check the log file for errors at $::test_log");
    }
    else { # return_code == 2) {
        error("Test run exited abnormally; please check the log file for errors at $::test_log");
    }
}

1;
