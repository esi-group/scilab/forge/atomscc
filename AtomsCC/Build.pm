package AtomsCC::Build;

# This file is part of the Scilab Atoms Compilation Chain
#
# Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Copyright (C) 2009-2010 - DIGITEO - Pierre MARECHAL
# Copyright (C) 2016 - Scilab Enterprises
#

use strict;
use Exporter;
use Cwd;
use File::Path;
use File::Basename;
use File::Copy;

use AtomsCC::Log;
use AtomsCC::SysUtils;

our @ISA = ('Exporter');
our @EXPORT = qw( &stage_Build &stage_CleanForBinary);

sub stage_Build
{
    my $compilation_path = $_[0];
    chdir($compilation_path);
    
    copy($::root.$::sep.'scripts'.$::sep.'atomsBuilder.sce', $compilation_path);

    my $cmd = '';
    if($::WINDOWS){
        $cmd = '"'.find_scilab().'" -l en_US';
    }
    else{
        $cmd = 'LANG=en_US.UTF-8 "'.find_scilab().'"';
    }

    my $return_code = timer('compilation (running scripts/atomsBuilder.sce)',
                            $compilation_path,
                            $cmd.' -nw -nb -f atomsBuilder.sce',
                            $::build_log);
    unless ($return_code == 0){
        error("Compilation failed, for details see $::build_log");
    }
        
    verbose('Compilation succeeded');
}

my %packages_items;

sub stage_CleanForBinary
{
    verbose('Cleaning the compiled toolbox to prepare the packaging');
    
    my $compilation_path = $_[0];

    # Get the list of files and directories to remove
    directory_to_hash($compilation_path, $compilation_path, 0);
    
    # and remove them
    foreach my $item (sort keys %packages_items)
    {
        if($packages_items{$item} eq 'file - remove') {
            unless( unlink($item) ) {
                error('The file "'.$item.'" cannot be deleted');
            }
	    verbose("deleting file ".$item);
        }
        
        if($packages_items{$item} eq 'dir - remove') {
            unless( rmtree($item) ) {
                error('The directory "'.$item.'" cannot be deleted');
            }
	    verbose("deleting dir ".$item);
        }
    }
    
    verbose('Toolbox successfully cleaned');
    return 0;
}

# Mark all files or directories to delete
sub directory_to_hash
{
    my $root   = $_[0];
    my $dir   = $_[1];
    my $level = $_[2];
    
    my @list_dir;
    my $current_directory;
    
    # On enregistre le répertoire dans lequel on se situe à l'entrée de la fonction
    my $previous_directory = getcwd();
    
    chdir($dir);
    
    @list_dir = <*>;
    
    foreach my $list_dir (@list_dir)
    {
        $current_directory = getcwd();
        my $current_file = $current_directory.$::sep.$list_dir;
        # Process directories
        
        if(-d $list_dir)
        {
            if($level == 0)
            {
                # Directories to keep
                if(($list_dir eq 'demos')  ||
                   ($list_dir eq 'jar')    ||
                   ($list_dir eq 'etc')    ||
                   ($list_dir eq 'tests') )
                {
                    next;
                }
                
                # Directories to remove
                if(($list_dir eq 'help')   ||
                   ($list_dir eq 'include') )
                {
                    $packages_items{$current_file} = 'dir - remove';
                    next;
                }
            }
            else{
		if($list_dir eq 'Release')
		{
                    $packages_items{$current_file} = 'dir - remove';
                    next;
		}
	    }
            # Directories to explore
            directory_to_hash($root, $current_file, $level+1);
        }
        
        # Process files
        
        if( -f $list_dir )
        {
            # Temporary files
            # ===============================
            
            if($list_dir =~ m/~$/)
            {
                $packages_items{$current_file} = 'file - remove';
                next;
            }
            
            if($level == 0)
            {
                # Parmi les .sce, seul le loader.sce doit rester... et le unloader.sce !! il n'y etait pas !!
                if( ($list_dir =~ m/\.sce$/) && ($list_dir ne 'loader.sce') && ($list_dir ne 'unloader.sce') )
                {
                    $packages_items{$current_file} = 'file - remove';
                    next;
                }
                
                # suppression des .*
                if( $list_dir =~ m/^\./ )
                {
                    $packages_items{$current_file} = 'file - remove';
                    next;
                }
                
                # suppression des atoms*
                if( ($list_dir =~ m/^atoms/) && ($list_dir ne 'loader.sce') )
                {
                    $packages_items{$current_file} = 'file - remove';
                    next;
                }
            }
            else
            {
                # Get the main directory ( ie : src/c/toto.c => src )
                my $rootdir = substr( $current_file , length($root.$::sep) );
                my $slash   = index($rootdir,'/');
                
                if($slash != -1)
                {
                    $rootdir = substr( $rootdir , 0 , $slash );
                }
                

                # Macros
                # ===============================
                
                if( ($rootdir eq 'macros') && ($::KEEPSOURCES == 0) )
                {
		    # remove the sci files (for which there are .bin),
		    # but not the sce (for which there are not .bin)
                    if($list_dir =~ m/\.sci$/)
                    {
                    	$packages_items{$current_directory.'/'.$list_dir} = 'file - remove';
			next;
                    }
                }
                
                # src/sci_gateway
                # ===============================
                
                if( (($rootdir eq 'src') || ($rootdir eq 'sci_gateway')) && ($::KEEPSOURCES == 0) )
                {
                    if( ($list_dir =~ m/\.c$/)
                       || ($list_dir =~ m/\.cxx$/)
                       || ($list_dir =~ m/\.cpp$/)
                       || ($list_dir =~ m/\.f$/) 
        		       || ($list_dir =~ m/\.obj$/)
        		       || ($list_dir =~ m/\.o$/)
        		       || ($list_dir =~ m/\.def$/)
        		       || ($list_dir =~ m/\.exp$/)
        		       || ($list_dir =~ m/\.lib$/)
        		       || ($list_dir =~ m/\.mak$/)
        			)
                    {
                        $packages_items{$current_file} = 'file - remove';
                        next;
                    }
                    
                    if( ($list_dir =~ m/\.sce$/) && ! ($list_dir =~ m/^loader(.)*\.sce$/ ) )
                    {
                        $packages_items{$current_file} = 'file - remove';
                        next;
                    }
                }
            }
        }
    }
    
    chdir($previous_directory);
}

1;
