// This file is part of the Scilab Atoms Compilation Chain
//
// Scilab Atoms Compilation Chain is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// 
// Copyright (C) 2016 - Scilab Enterprises

funcprot(0);

//old_ilib_compile = ilib_compile;

function libn = _ilib_compile(lib_name,makename,files,ldflags,cflags,fflags)
	libn = old_ilib_compile(lib_name,makename,files,ldflags,cflags,fflags);
	mprintf("\natoms_cc_ilib_compile:%s/%s\n", pwd(), libn);
endfunction

function %onprompt
	quit;
endfunction

//old_genlib = genlib;

function [success,funcs,success_files,failed_files] = _genlib(nam,path,force,verbose,names)
	
	if exists("names", "local") then
		[success,funcs,success_files,failed_files] = old_genlib(nam,path,force,verbose,names)
	elseif exists("verbose", "local") then
		[success,funcs,success_files,failed_files] = old_genlib(nam,path,force,verbose)
	elseif exists("force", "local") then
		[success,funcs,success_files,failed_files] = old_genlib(nam,path,force)
	elseif exists("path", "local") then
		[success,funcs,success_files,failed_files] = old_genlib(nam,path)
	else
		[success,funcs,success_files,failed_files] = old_genlib(nam)
	end
	
	mprintf("\natoms_cc_genlib:%i\n", int32(success));
//	mprintf("atoms_cc_genlib_funcs:%s\n", strcat(funcs,","));
	disp("atoms_cc_genlib_funcs:");
	disp(strcat(funcs,","));
	
endfunction

ierr=exec("builder.sce","errcatch");

if(ierr <> 0) then 
	mprintf("\nBuild Error %d\n",ierr);
	disp(lasterror());
else
	mprintf("\natoms_cc_builder:done\n");
end
exit(ierr);
